(function() {

// Chart design based on the recommendations of Stephen Few. Implementation
// based on the work of Clint Ivy, Jamie Love, and Jason Davies.
// http://projects.instantcognition.com/protovis/bulletchart/
d3.bullet = function() {
  var orient = "left", // TODO top & bottom
      reverse = false,
      duration = 0,
      ranges = bulletRanges,
      markers = bulletMarkers,
      measures = bulletMeasures,
      measureHeight = bulletMeasureHeight,
	  rangeHeight = bulletRangeHeight,
	  axisOrder = bulletAxisOrder,
      width = 380,
      height = 30,
      tickFormat = null,
	  rangeColorArray = [],
      maxTickHeight;
  // For each small multiple…
  function bullet(g) {
	
    g.each(function(d, i) {
      var rangez = ranges.call(this, d, i).slice().sort(d3.descending),
          markerz = markers.call(this, d, i).slice().sort(d3.descending),
          measurez = measures.call(this, d, i).slice().sort(d3.descending),
          measureHeightz = measureHeight.call(this, d, i).slice().sort(d3.descending),
		  rangeHeightz = rangeHeight.call(this, d, i).slice().sort(d3.descending),
          rangeMax = getRangeMax.call(this, d, i).slice().sort(d3.descending),
		  axisOrderz = axisOrder.call(this, d, i).slice().sort(d3.descending),
          g = d3.select(this);
		
		
		reverse = ((axisOrderz[0] == "high") ? false : true) ;
		
		var minVal = Math.min(rangez[rangez.length-1], ((markerz.length ==0) ? 0 : markerz[markerz.length-1]), measurez[measurez.length-1]);
		var maxVal = Math.max(rangez[0], ((markerz.length ==0) ? 0 : markerz[0]), measurez[0]);
      //set the x-axis scale based on the menu setting for universal vs independent
      if (rangeMax==0) {
		 //console.log(width);
		 //minVal=0;
		  // Compute the new x-scale.
		  var minDomain = ((reverse) ? minVal : (minVal - (0.1 * minVal))), 
			maxDomain = ((reverse) ? maxVal : (maxVal + (0.1 * maxVal)));
			//console.log(minDomain);
            var x1 = d3.scale.linear()
                .domain([minDomain, maxDomain])
                .range(reverse ? [width, minVal] : [minVal, width]);
      } else {
            // Compute the new x-scale.
            var x1 = d3.scale.linear()
                .domain([0, rangeMax])
                .range(reverse ? [width, 0] : [0, width]);
      }
	  
	  
      var x0 = this.__chart__ || d3.scale.linear()
          .domain([minVal, Infinity])
          .range(x1.range());

      // Stash the new scale.
      this.__chart__ = x1;
		
      // Derive width-scales from the x-scales.
      var w0 = bulletWidth(x0),
          w1 = function(d){
			  return ((reverse) ? Math.abs(x1(d) - x1(rangez[0])) : Math.abs(x1(d) - x1(minVal)));
			 };
	
		//create labels for each bullet
		var title = g.append("g")
		  .style("text-anchor", "end")
		  .attr("transform", "translate(0," + height/2 + ")");

		title.append("text")
		  .attr("class", "title")
		  .text(function(d) { return d.title; });
	
      // Update the range rects.
      var range = g.selectAll("rect.range")
          .data(rangez.filter(function(d,i) { return i != rangez.length-1; }));

      range.enter().append("rect")
          .attr("class", function(d, i) { return "range s" + i; })
		  .attr("fill", function(d, i) { return (reverse) ? rangeColorArray[(rangez.length - i -2)] :rangeColorArray[i]; })
          .attr("width", function(d) {
					return Math.abs(x1(d) - (x1(rangez[rangez.length-1])));
				  }
			)
          .attr("height", height * rangeHeightz[0] * .01);
         
      range.transition()
          .duration(duration)
          .attr("x", reverse ? x1 : x1(rangez[rangez.length-1]))
		  .attr("y", height * (100-rangeHeightz[0]) * .005);
		
      // Update the measure rects.
      var measure = g.selectAll("rect.measure")
          .data(measurez);

      measure.enter().append("rect")
          .attr("class", function(d, i) { return "measure s" + i; })
          .attr("width", w1)
          .attr("height", height * measureHeightz[0] * .01);
		
      measure.transition()
          .duration(duration)
          .attr("width", w1)
          .attr("height", height * measureHeightz[0] * .01)
          .attr("x", reverse ? x1(rangez[0]) : x1(minVal))
          .attr("y", height * (100-measureHeightz[0]) * .005);//use user defined measure height divided by two for y positioning

      // Update the marker lines.
      var marker = g.selectAll("line.marker")
          .data(markerz);

      marker.enter().append("line")
          .attr("class", "marker")
          .attr("x1", x0)
          .attr("x2", x0)
          .attr("y1", height / 2.8)
          .attr("y2", height * .65)

      marker.transition()
          .duration(duration)
          .attr("x1", x1)
          .attr("x2", x1)
          .attr("y1", height / 2.8)
          .attr("y2", height * .65);
      // Compute the tick format.
      var format = tickFormat || x1.tickFormat(8);

      // Ensure max tick height is less than 11
      if ((height*1.05)>8) {
        maxTickHeight = 8;
      }
      else {
        maxTickHeight = height*1.05;
      };
		
      // Update the tick groups.
      var tick = g.selectAll("g.tick")
          .data(x1.ticks(8), function(d) {
            return this.textContent || format(d);
          });
	  var tickHeightPosition =   height-20;
      // Initialize the ticks with the old scale, x0.
      var tickEnter = tick.enter().append("g")
          .attr("class", "tick")
          .attr("transform", bulletTranslate(x0))
          .style("opacity", 1e-6);
		  
      tickEnter.append("line")
          .attr("y1", tickHeightPosition)
          .attr("y2", tickHeightPosition + maxTickHeight);
      tickEnter.append("text")
          .attr("text-anchor", "middle")
          .attr("dy", "1em")
          .attr("y", tickHeightPosition + maxTickHeight)
          .text(format);
		
		  
      // Transition the entering ticks to the new scale, x1.
      tickEnter.transition()
          .duration(duration)
          .attr("transform", bulletTranslate(x1))
          .style("opacity", 1);

      // Transition the updating ticks to the new scale, x1.
      var tickUpdate = tick.transition()
          .duration(duration)
          .attr("transform", bulletTranslate(x1))
          .style("opacity", 1);

      tickUpdate.select("line")
          .attr("y1", tickHeightPosition)
          .attr("y2", tickHeightPosition + maxTickHeight);

      tickUpdate.select("text")
          .attr("y", tickHeightPosition + maxTickHeight);

      // Transition the exiting ticks to the new scale, x1.
      tick.exit().transition()
          .duration(duration)
          .attr("transform", bulletTranslate(x1))
          .style("opacity", 1e-6)
          .remove();
	
		// draw line over axis
	   var lineG = g.append("g")
		.attr("class", "tick")
		.attr("transform", "translate(0," + (height - 20) + ")")
		.style("opacity", 1);
		
		lineG.append("line")
		.attr("x1", 0)
		.attr("y1", 0)
		.attr("x2", ((reverse) ? x1(0) : x1(maxDomain)))
		.attr("y2", 0);
			
    });
    d3.timer.flush();
  }

  // left, right, top, bottom
  bullet.orient = function(x) {
    if (!arguments.length) return orient;
    orient = x;
    reverse = orient == "right" || orient == "bottom";
    return bullet;
  };

  // ranges (bad, satisfactory, good)
  bullet.ranges = function(x) {
    if (!arguments.length) return ranges;
    ranges = x;
    return bullet;
  };

  // markers (previous, goal)
  bullet.markers = function(x) {
    if (!arguments.length) return markers;
    markers = x;
    return bullet;
  };

  // measures (actual, forecast)
  bullet.measures = function(x) {
    if (!arguments.length) return measures;
    measures = x;
    return bullet;
  };

  bullet.width = function(x) {
    if (!arguments.length) return width;
    width = x;
    return bullet;
  };

  bullet.height = function(x) {
    if (!arguments.length) return height;
    height = x;
    return bullet;
  };

  bullet.tickFormat = function(x) {
    if (!arguments.length) return tickFormat;
    tickFormat = x;
    return bullet;
  };

  bullet.duration = function(x) {
    if (!arguments.length) return duration;
    duration = x;
    return bullet;
  };
  
  bullet.rangeColors = function(x) {
    if (!arguments.length) return rangeColorArray;
    rangeColorArray = x;
    return bullet;
  };

  return bullet;
};

function bulletMeasureHeight(d){
  return d.measureBarHeight;
}

function bulletRangeHeight(d){
  return d.rangeBarHeight;
}

function getRangeMax (d){
  return d.rangeMax;
}

function bulletRanges(d) {
  return d.ranges;
}

function bulletMarkers(d) {
  return d.markers;
}

function bulletMeasures(d) {
  return d.measures;
}

function bulletAxisOrder(d) {
  return d.axisOrder;
}

function bulletTranslate(x) {
  return function(d) {
    return "translate(" + x(d) + ", 0)";
  };
}

function bulletWidth(x) {
  var x0 = x(0);
  return function(d) {
	 // console.log(d);
    return Math.abs(x(d) - x0);
  };
}
})();