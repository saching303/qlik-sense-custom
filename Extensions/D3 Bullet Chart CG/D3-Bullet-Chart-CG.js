requirejs.config({
    shim: {
      //Load d3 library before bullet.js
      //Only seems to work when extension name is hard coded
        "extensions/D3-Bullet-Chart-CG/bullet": ["extensions/D3-Bullet-Chart-CG/d3.min"]
    }
});

define( ["jquery","qlik","./bullet","css!./stylesheet.css"], function ($,qlik,bullet) {
	
        'use strict';
		
		var colorOptions = [{
            value: "#00FFFF",
            label: "Aqua"  
        },{
            value: "#000000",
            label: "Black"  
        },{
            value: "#A52A2A",
            label: "Brown"  
        },{
            value: "#D2691E",
            label: "Chocolate"  
        },{
            value: "#FF7F50",
            label: "Coral"  
        },{
            value: "#DC143C",
            label: "Crimson"  
        },{
            value: "#00008B",
            label: "Dark Blue"  
        },{
            value: "#006400",
            label: "Dark Green"  
        },{
            value: "#FF8C00",
            label: "Dark Orange"  
        },{
            value: "#696969",
            label: "Dim Grey"  
        },{
            value: "#B22222",
            label: "Fire Brick"  
        },{
            value: "#228B22",
            label: "Forest Green"  
        },{
            value: "#008000",
            label: "Green"  
        },{
            value: "#808080",
            label: "Grey"  
        },{
            value: "#ADD8E6",
            label: "Light Blue"  
        },{
            value: "#D3D3D3",
            label: "Light Grey"  
        },{
            value: "#00FF00",
            label: "Lime"  
        },{
            value: "#800000",
            label: "Maroon"  
        },{
            value: "#4169E1",
            label: "Royal Blue"  
        },{
            value: "#8B4513",
            label: "Saddle Brown"
        },{
            value: "#F4A460",
            label: "Sandy Brown"
        },{
            value: "#2E8B57",
            label: "Sea Green"  
        },{
            value: "#708090",
            label: "Slate Grey"  
        },{
            value: "#4682B4",
            label: "Steel Blue"  
        },{
            value: "#008080",
            label: "Teal"  
        },{
            value: "#D2B48C",
            label: "Tan"  
        },{
            value: "#EE82EE",
            label: "Violet"  
        },{
            value: "#F5DEB3",
            label: "Wheat"  
        }
        ];

    //----------individual accordion labels-------------
    //Dimensions
    var dimLabel = {
        ref: "props.section1.dimLabel",
        label: "Dimension Label",
        type: "string",
        expression: "optional"
    };

    var showDimSubTitles = {
        ref : "props.section1.showDimSubTitles",
        label : "Display Subtitles",
        type : "boolean",
        defaultValue : false
    };
    var dimWidth = {
        ref : "props.section1.dimWidth",
        label : "Change space for dimension text",
        type : "number",
        defaultValue : 80,
        min : 10,
        max : 200,
        component : "slider"
    };
    //Measure configuration
    var barSize = {
        ref : "props.section2.barSize",
        label : "Change size of bar",
        type : "number",
        defaultValue : 15,
        min : 5,
        max : 40,
        component : "slider"
    };
    var barColor = {
        ref : "props.section2.barColor",
        label : "Change bar color",
        type : "string",
        defaultValue: "#4682B4",
        component: "dropdown",
        options: colorOptions
    };
    //Marker configuration
    var markerColor = {
        ref : "props.section3.markerColor",
        label : "Change marker color",
        type : "string",
        defaultValue: "#000000",
        component: "dropdown",
        options: colorOptions
    };
    //Range configuration
    var rangeColor = {
        ref : "props.section4.rangeColor",
        label : "Change range color",
        type : "string",
        defaultValue: "#D3D3D3",
        component: "dropdown",
        options: colorOptions
    };
    var middleThreshRangeColor = {
        ref : "props.section4.middleThreshRangeColor",
        label : "Change middle range gradient",
        type : "integer",
        defaultValue: 0.7,
        min : 0,
        max : 1.55,
        step : .05,
        component: "slider"
    };
    var lowerThreshRangeColor = {
        ref : "props.section4.lowerThreshRangeColor",
        label : "Change lower range gradient",
        type : "integer",
        defaultValue: 0.85,
        min : 0,
        max : 1.55,
        step : .05,
        component: "slider"
    };
    var lowerThreshRange = {
        ref : "props.section4.lowerThreshRange",
        label : "Change lower range",
        type : "integer",
        defaultValue: 0.5,
        min : 0,
        max : 1.05,
        step : .05,
        component: "slider"
    };
    var middleThreshRange = {
        ref : "props.section4.middleThreshRange",
        label : "Change middle range",
        type : "integer",
        defaultValue: 0.75,
        min : 0,
        max : 1.05,
        step : .05,
        component: "slider"
    };
    var upperThreshRange = {
        ref : "props.section4.upperThreshRange",
        label : "Change upper range",
        type : "integer",
        defaultValue: 1,
        min : 0,
        max : 1.05,
        step : .05,
        component: "slider"
    };
    //Axis configuration
    var uniformAxisBool = {
        ref : "props.section5.uniformAxisBool",
        label : "Consistent axis for all dimensions",
        type : "boolean",
        defaultValue : false
    };

        //Function that returns 0 if the value passed is NaN or less than 0
        var validateBulletNums = function (val){
          var enteredVal = Number(val),
            finalVal=0;
          if (enteredVal>0) {
            finalVal=enteredVal;
          }
          return enteredVal;
        };

        //Function to convert hex value to rgb array
        var hexToRgb = function (hex) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        };

        //Function to take hypercube data and turn it into d3 readable array
        var createDataArray = function (hypercubeData, layout){
        
          //get dimension label if it exists, if not create an empty string
          if (layout.props.section1.dimLabel) { 
            var dimLabel=layout.props.section1.dimLabel;
          }
          else {
            var dimLabel=""
          };
          //create variables from layout settings
          var propShowDimSubTitles  = layout.props.section1.showDimSubTitles,
              propMeasureBarSize    = layout.props.section2.barSize,
              propUpperRangeThresh  = Number(layout.props.section4.upperThreshRange),
              propMiddleRangeThresh = Number(layout.props.section4.middleThreshRange),
              propLowerRangeThresh  = Number(layout.props.section4.lowerThreshRange),
              propUniformAxis       = layout.props.section5.uniformAxisBool;


          //final array creation, create variables for testing and data manipulation as well
          var dataObject = [],
              numMeasures = hypercubeData.qMeasureInfo.length,
              numDims     = hypercubeData.qDimensionInfo.length,
              dataPages  = hypercubeData.qDataPages[0].qMatrix;
          var rangeMax =0;
		  
          //loop through all rows in data cube
          for (var r = 0; r < dataPages.length; r++) {
			
			if (dataPages[r][0].qIsNull){
				continue;
			}
			
            //use dimensions if one was created
            if (numDims!==0) {
              dataObject.push({ "title"   : dataPages[r][0].qText});
            }
            else {//if no dimensions were added, use the title listed
              dataObject.push({ "title"   : dimLabel});
            }
			
			dataObject[r]["subtitle"]  = ((numDims > 2) ? dataPages[r][2].qText : "");  
			dataObject[r]["axisOrder"] = ((numDims > 1) ? [dataPages[r][1].qText] : ["high"]);
			
			var dimQuantiles=[], quantileIndexer = (numDims + 2);
			
			while(quantileIndexer < (numDims + numMeasures)){
				dimQuantiles.push(validateBulletNums(dataPages[r][quantileIndexer].qNum));
				quantileIndexer++;
			}
                   
			dataObject[r]["measures"] = [validateBulletNums(dataPages[r][numDims].qText.replace(",",""))];
			dataObject[r]["markers"]  = ((numMeasures == 1) ? []: [validateBulletNums(dataPages[r][numDims+1].qText.replace(",",""))]);
			dataObject[r]["ranges"]   = dimQuantiles;

			//create the measure bar height as an additional data measure, this is driven from properties
			dataObject[r]["measureBarHeight"] = [propMeasureBarSize];
			dataObject[r]["rangeBarHeight"] = [40];
			
			 if (propUniformAxis) {
              //Find the biggest number in the current array and compare it to 
              if (Math.max(dataObject[r]["measures"],dataObject[r]["markers"],dataObject[r]["ranges"][0]) > rangeMax) {
                rangeMax = Math.max(dataObject[r]["measures"],dataObject[r]["markers"],dataObject[r]["ranges"][0]);
              }
            }

          }
		  
			for (var r = 0; r < dataPages.length; r++) {
				dataObject[r]["rangeMax"]  = [rangeMax];
			}

          return dataObject;
        };

        return {
            definition: {
				type: "items",
				component: "accordion",
				items: {
					dimensions: {
						uses: "dimensions"
						,min: 1
						,max: 3
					},
					measures: {
						uses: "measures"
						,min: 1
						,max: 7
					},
					sorting: {
						uses: "sorting"
					},
					appearance: {
						uses: "settings"
					},
					configuration : {
							component: "expandable-items",
							label: "Chart Configuration",
							items: {
								header1: {
									type: "items",
									label: "Dimensions",
									items: {
										dimensionLabel:         dimLabel,
										dimensionTitleBtn :     showDimSubTitles,
										dimWidth:               dimWidth
									}
								},
								header2: {
									type: "items",
									label: "Measure Bar",
									items: {
										barSize:                barSize,
										barColor:               barColor
									}
								},
								header3: {
									type: "items",
									label: "Marker",
									items: {
										markerColor:            markerColor
									}
								},
								header4: {
									type: "items",
									label: "Range",
									items: {
										rangeColor:             rangeColor,
										//lowerThreshRangeColor:  lowerThreshRangeColor,
										//middleThreshRangeColor: middleThreshRangeColor,
										upperThreshRange:       upperThreshRange,
										middleThreshRange:      middleThreshRange,
										lowerThreshRange:       lowerThreshRange
									}
								},
								header5: {
									type: "items",
									label: "Axis",
									items: {
										uniformAxisBool:        uniformAxisBool
									}
								}
							}
					}
				}
			},
            initialProperties: {
                    qHyperCubeDef: {
                      qDimensions: [],
                      qMeasures: [],
                      qInitialDataFetch: [
                          {
                              qWidth: 10,
                              qHeight: 1000
                          }
                      ]
                    }
            },
            paint: function ( $element, layout ) {
				
				//convert hex to rgb as first step of gradient creation
				var rangeRGB          = hexToRgb(layout.props.section4.rangeColor),
					lastRangeThresh = .50,
					lowerRangeThresh  = .7, //(layout.props.section4.lowerThreshRangeColor),
					midRangeThresh = .85; //(layout.props.section4.middleThreshRangeColor);
				
				var rangeColorArray = [
				"rgb("+rangeRGB.r+", "+rangeRGB.g+", "+rangeRGB.b+")",
				"rgb("+Math.floor(rangeRGB.r*midRangeThresh)+", "+Math.floor(rangeRGB.g*midRangeThresh)+", "+Math.floor(rangeRGB.b*midRangeThresh)+")",
				"rgb("+Math.floor(rangeRGB.r*lowerRangeThresh)+", "+Math.floor(rangeRGB.g*lowerRangeThresh)+", "+Math.floor(rangeRGB.b*lowerRangeThresh)+")",
				"rgb("+Math.floor(rangeRGB.r*lastRangeThresh)+", "+Math.floor(rangeRGB.g*lastRangeThresh)+", "+Math.floor(rangeRGB.b*lastRangeThresh)+")"
				];
				
				//set hypercube variable and call function on hcData to return data in a json format
				var hc = layout.qHyperCube,
					hcData = createDataArray(hc,layout);
				
				//hcData.rangeColors = rangeColorArray;
				
				//create variables for number of bars allowed and the size of the dimension area for text
				var numOfBarsAllowed,
					dimWidth=Number(layout.props.section1.dimWidth);
				//check that not too many bars are trying to be displayed
				if (Math.floor($element.height()/75)===0) {
				  numOfBarsAllowed = 1
				} else {
				  numOfBarsAllowed = Math.floor($element.height()/75)
				}

				//if there are more dimensions in the array than the number of bars allowed then reduce the size of the array
				if (hcData.length>numOfBarsAllowed) {
				  hcData.splice(numOfBarsAllowed,hcData.length-numOfBarsAllowed);
				};

				// Create margin - should be replaced by dynamic numbers when this is eventually a responsive viz
				var margin = {top: 5, right: 20, bottom: 25, left: dimWidth};

				// Set chart object width
				var width = $element.width() - margin.left - margin.right;

				// Set chart object height
				var height = ($element.height()/hcData.length) - margin.top - margin.bottom;// - hcData.length*10;//subtract addtl for bottom margin clipping

				// Chart object id
				var id = "container_" + layout.qInfo.qId;

				// Check to see if the chart element has already been created
				if (document.getElementById(id)) {
					// if it has been created, empty it's contents so we can redraw it
					$("#" + id).empty();
				} else {
					// if it hasn't been created, create it with the appropiate id and size
					$element.append($('<div />').attr("id", id).attr("class","divbullet").width(width).height(height));
				}
  
				var chart = d3.bullet()
					.width(width)
					.height(height)
					.rangeColors(rangeColorArray);

				var svg = d3.select("#" + id).selectAll("svg")
				  .data(hcData)
				.enter().append("svg")
				  .attr("class", "bullet")
				  .attr("width", width + margin.left + margin.right)
				  .attr("height", height + margin.top + margin.bottom - 2)
				.append("g")
				  .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
				  .call(chart);
				/*
				//create labels for each bullet
				var title = svg.append("g")
				  .style("text-anchor", "end")
				  .attr("transform", "translate(-6," + height/2 + ")");

				title.append("text")
				  .attr("class", "title")
				  .text(function(d) { return d.title; });
				*/
				var subtitle = svg.append("g")
				.style("text-anchor", "end")
				.attr("transform", "translate(" + width/2 +", "+ (height+20) + ")")
				 
				subtitle.append("text")
				.attr("class", "subtitle")
				.text(function (d) { return d.subtitle; });
				
				//fill the bullet with the color specified in the menu
				$("#" + id+" rect.measure").attr("fill",layout.props.section2.barColor);

				//color the marker with the color specified in the menu
				$("#" + id+" line.marker").attr("stroke",layout.props.section3.markerColor);
            },
            resize: function ($el, layout) {
                this.paint($el, layout);
            }
        };
    } );