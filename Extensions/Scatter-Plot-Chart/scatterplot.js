define(["jquery", "qlik","text!./scatter.css","./d3.v3.min", "./scatterUtils","./underscore-min"], 
function($, qlik, cssContent) {
	'use strict';
    $("<style>").html(cssContent).appendTo("head");
	var control = 0;
    return {
        initialProperties : {
            version: 1.0,
            qHyperCubeDef : {                
                qDimensions : [],
                qMeasures : [],
                qInitialDataFetch : [{
                    qWidth : 5,
                    qHeight : 1000
                }]
            }
        },
        definition : {
            type : "items",
            component : "accordion",
            items : {
                dimensions : {
                    uses : "dimensions",
                    min : 1,
                    max: 3
                },
                measures : {
                    uses : "measures",
                    min : 2,
                    max: 4
                },
                sorting : {
                    uses : "sorting"
                },
                settings : {
                    uses : "settings"
                }
            }
        },
        support: {
          snapshot: true,
          export: true,
          exportData: true
        },        
        paint : function($element,layout) {   
            console.log("variable-based");
            //console.log(layout);
			if (control == 0){
				//this.backendApi.selectValues(1, [0], false);
				qlik.currApp().variable.setContent("vSelectedYear","$(vCurrentYear)");
		
				control++;
			}

            var self = this;  
			
			var requestPage = [{
				qTop : 0 + 1,
				qLeft : 0,
				qWidth : 10, //should be # of columns
				qHeight : Math.min(50, this.backendApi.getRowCount() - 0)
			}];
		
		/*	
			self.backendApi.getData(requestPage).then(function(dataPages) {
				
				//console.log(dataPages);
			});
		*/
            
            senseUtils.extendLayout(layout, self);
            
            viz($element, layout, self,qlik);
			
			
        
        },
        resize:function($el,layout){
        this.paint($el,layout);
      }
    };
});


var viz = function($element, layout, _this, _qlik) {
	
//	console.log(layout.defaultselection);
	//console.log(layout);
	
  var id = senseUtils.setupContainer($element,layout,"scatter"),
    ext_width = $element.width(),
    ext_height = $element.height(),
    classDim = layout.qHyperCube.qDimensionInfo[0].qFallbackTitle.replace(/\s+/g, '-');    
//console.log(layout.qHyperCube);
  var masterData = layout.qHyperCube.qDataPages[0].qMatrix,
  data = masterData;
	//console.log(layout.qHyperCube.qSize);
  var margin = {top: 50, right: 100, bottom: 50, left: 50 },
      width = ext_width - margin.left - margin.right,
      height = ext_height - margin.top - margin.bottom;

  var x = d3.scale.linear()
      .range([0, width]);
  var y = d3.scale.linear()
      .range([height, 0]);

  var xMax = d3.max(data, function(d) { return d.measure(1).qNum; })*1.02,
	
    xMin = d3.min(data, function(d) { return d.measure(1).qNum; })*0.98,
	
    yMax = d3.max(data, function(d) { return d.measure(2).qNum; })*1.02,
	
    yMin = d3.min(data, function(d) { return d.measure(2).qNum; })*0.98;
	
    var xMin2 = xMin == xMax ? xMin*0.5 : xMin;
    var xMax2 = xMin == xMax ? xMax*1.5 : xMax;
    var yMin2 = yMin == yMax ? yMin*0.5 : yMin;
    var yMax2 = yMin == yMax ? yMax*1.5 : yMax;
   
     x.domain([xMin2, xMax2]).nice();
     y.domain([yMin2, yMax2]).nice();    
   
  var color = d3.scale.category20();

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .tickSize(-height)
      .tickFormat(d3.format(".2s")); 

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")  
      .tickSize(-width)       
      .tickFormat(d3.format(".2s"));

  var zoomBeh = d3.behavior.zoom()
      .x(x)
      .y(y)
      .scaleExtent([0, 500])
      .on("zoom", zoom);

   var resetZoomButton = d3.select("#" + id) 
      .append("input")
	  .attr("Id", "btnResetZoom")
      .attr("type", "button")
      .attr("name", "resetZoom")
      .attr("value", "Reset Zoom")
      .attr("float", "left"); 
	  
	var showHistoryButton = d3.select("#" + id) 
		.append("input")
		.attr("Id", "btnShowHistory")
		.attr("type", "button")
		.attr("name", "showHistory")
		.attr("value", "Show History")
		.attr("float", "left"); 
		
	var resetButton = d3.select("#" + id) 
      .append("input")
	  .attr("Id", "btnReset")
      .attr("type", "button")
      .attr("name", "reset")
      .attr("value", "Reset")
      .attr("float", "left"); 

  var svg = d3.select("#" + id)    
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
     .attr("transform", "translate(" + margin.left + "," + margin.top + ")") 
     .call(zoomBeh);

  svg.append("rect")
    .attr("width", width)
    .attr("height", height);

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .append("text")
    .attr("class", "label")
    .attr("x", width)
    .attr("y", margin.bottom - 10)
    .style("text-anchor", "end")
    .text(senseUtils.getMeasureLabel(1,layout));

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .append("text")
    .attr("class", "label")
    .attr("transform", "rotate(-90)")
    .attr("y", -margin.left)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text(senseUtils.getMeasureLabel(2,layout));  

  var plot = svg.append("svg")
    .classed("objects", true)
      .attr("width", width)
      .attr("height", height);    
	
	
	plotChartWith(data, transform);
	
    prepareLegends();
	
	highLightHistory(_this);
	
    d3.select("#btnResetZoom").on("click", change);

	d3.select("#btnShowHistory").on("click", showHistory);
	
	d3.select("#btnReset").on("click", function(){
		
		_qlik.currApp().variable.setContent("vSelectedYear","$(vCurrentYear)");
		//_this.backendApi.clearSelections();
		//_this.backendApi.selectValues(1, [0], false);
		
	});
	
	function plotChartWith(data, fncTransform){
		
		 plot.selectAll(".dot")
        .data(data)
      .enter().append("circle")
        .attr("class", "dot "+classDim)
        .attr("transform", fncTransform)
        .attr("id", function(d) { return d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-"); })
        .attr("r", function(d) { return  5; })
        //.attr("cx", function(d) { return x(d.measure(1).qNum); })
        //.attr("cy", function(d) { return y(d.measure(2).qNum); })
        .style("fill", function(d) { return color(d.dim(1).qText); })
        .on("click", function(d) {d.dim(1).qSelect();})
        .on("mouseover", function(d){
          d3.selectAll($("."+classDim+"#"+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-"))).classed("highlight",true);
              d3.selectAll($("."+classDim+"[id!="+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-")+"]")).classed("dim",true);
          })
          .on("mouseout", function(d){
              d3.selectAll($("."+classDim+"#"+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-"))).classed("highlight",false);
              d3.selectAll($("."+classDim+"[id!="+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-")+"]")).classed("dim",false);
          })
            .append("title")
            .html(function(d) {return senseUtils.getDimLabel(1,layout) + ": " + d.dim(1).qText 
                    + "<br/>" + senseUtils.getMeasureLabel(1,layout) + ": " + d.measure(1).qText
                    + "<br/>" + senseUtils.getMeasureLabel(2,layout) + ": " + d.measure(2).qText  
                    + "<br/>" + senseUtils.getMeasureLabel(2,layout) + " (Bubble): " + "0"//d.measure(3).qText
                      });
    
	}
	
	function showHistory(){
		//_this.backendApi.selectValues(1, [1], true);
		var currSel;
		_qlik.currApp().variable.getContent('vSelectedYear',function ( reply ) {
				currSel = reply ;
				console.log(reply.qContent.qString);
		} );
		
		_qlik.currApp().variable.setContent("vSelectedYear","$(vCurrentYear),$(vLastYear)");
		
		
		/*
		plot.append("marker")
		.attr("id","arrow1")
		.attr("refX",100)
		.attr("refY",100)
		.attr("markerWidth",30)
		.attr("markerHeight",30)
		.attr("orient","auto")
		.append("path")
		.attr("d","M 0 0 12 6 0 12 3 6")
		.style("fill", "black")
		*/
	}
	
	function prepareLegends(){
		 var legend = svg.selectAll(".legend")
        .data(color.domain())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });           
    

    legend.append("circle")
        .attr("r", 5)
        .attr("cx", width + 25)
        .attr("fill", color);

    legend.append("text")
        .attr("x", width + 32)
        .attr("dy", ".35em")
        .text(function(d) { return d; });
	}
	
    function change() {
		
      var xMax3 = d3.max(data, function(d) { return d.measure(1).qNum; })*1.02;
      var xMin3 = d3.min(data, function(d) { return d.measure(1).qNum; })*0.98;
      var yMax3 = d3.max(data, function(d) { return d.measure(2).qNum; })*1.02;
      var yMin3 = d3.min(data, function(d) { return d.measure(2).qNum; })*0.98;

      
      zoomBeh
        .x(x.domain([xMin3, xMax3]).nice())
        .y(y.domain([yMin3, yMax3]).nice());

      var svg = d3.select("#" + id).transition();

      svg.select(".x.axis").duration(750).call(xAxis).select(".label").text(senseUtils.getMeasureLabel(1,layout));
      svg.select(".y.axis").duration(750).call(yAxis).select(".label").text(senseUtils.getMeasureLabel(2,layout));

      plot.selectAll(".dot").transition().duration(1000).attr("transform", transform);
    }

    function zoom() {
      svg.select(".x.axis").call(xAxis);
      svg.select(".y.axis").call(yAxis);

      svg.selectAll(".dot")
          .attr("transform", transform);
    }
	
	function highLightHistory(self){
		
		var lineOption = { x1:0, y1:0, x2:0, y2: 0, reset: function(){
			
			this.x1	= 0;
			this.y1	= 0;
			this.x2	= 0;
			this.y2	= 0;
		}};
		
		//lineOption.reset();
		
		_.each(color.domain(),function(elem){
			var count = 0;
			lineOption.reset();
			
			_.each(data,function(d){
			   var dime = _.where(d, {qText: elem});
			  // console.log("for "+ elem + "=" + dime.length);
			   if(dime.length == 1){
				   if (count == 0){
					   lineOption.x2 = d[2].qText;
					   lineOption.y2 = d[3].qText;
				   }
				   if (count == 1){
					   lineOption.x1 = d[2].qText;
					   lineOption.y1 = d[3].qText;
				   }
				   count++;
			   }
			});
		//	console.log(count);
			
			if (count == 2){
				plot.append("line")
				.attr("x1",x(lineOption.x1))
				.attr("y1",y(lineOption.y1))
				.attr("x2",x(lineOption.x2))
				.attr("y2",y(lineOption.y2))
				.attr("stroke-width",2)
				.attr("stroke","black")
		
			}
			
		});
		
	}

    function transform(d) {    
      
      return "translate(" + x(d.measure(1).qNum) + "," + y(d.measure(2).qNum) + ")";

    }
}