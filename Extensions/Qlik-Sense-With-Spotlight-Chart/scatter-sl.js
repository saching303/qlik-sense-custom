define(["jquery", "text!./scatter-sl.css","./d3.v3.min", "./scatter-slUtils"], function($, cssContent) {'use strict';
    $("<style>").html(cssContent).appendTo("head");
    return {
        initialProperties : {
            version: 1.0,
            qHyperCubeDef : {                
                qDimensions : [],
                qMeasures : [],
                qInitialDataFetch : [{
                    qWidth : 5,
                    qHeight : 1000
                }]
            }
        },
        definition : {
            type : "items",
            component : "accordion",
            items : {
                dimensions : {
                    uses : "dimensions",
                    min : 1,
                    max: 1
                },
                measures : {
                    uses : "measures",
                    min : 2,
                    max: 4
                },
                sorting : {
                    uses : "sorting"
                },
                settings : {
                    uses : "settings"
                }
            }
        },
        support: {
          snapshot: true,
          export: true,
          exportData: true
        },        
        paint : function($element,layout) {   
            var self = this;  
			if (!self.options.showPriorMeaures){
				self.options.showPriorMeaures = false;
			}
            senseUtils.extendLayout(layout, self);
            viz($element, layout, self);
        },
        resize:function($el,layout){
        this.paint($el,layout);
      }
    };
});


var viz = function($element, layout, _this) {
	console.log(_this);
  var id = senseUtils.setupContainer($element,layout,"scatter"),
    ext_width = $element.width(),
    ext_height = $element.height(),
    classDim = layout.qHyperCube.qDimensionInfo[0].qFallbackTitle.replace(/\s+/g, '-'),
	spotLightData;

  var data = layout.qHyperCube.qDataPages[0].qMatrix;

  var margin = {top: 50, right: 50, bottom: 50, left: 50 },
      width = ext_width - margin.left - margin.right,
      height = ext_height - margin.top - margin.bottom;

  var x = d3.scale.linear()
      .range([0, width]);
  var y = d3.scale.linear()
      .range([height, 0]);

	  var xMax,xMin,yMax,yMin;
	  
	if (!_this.options.showPriorMeaures){
		 xMax = d3.max(data, function(d) { return d.measure(1).qNum; })*1.02;
		xMin = d3.min(data, function(d) { return d.measure(1).qNum; })*0.98;
		yMax = d3.max(data, function(d) { return d.measure(2).qNum; })*1.02;
		yMin = d3.min(data, function(d) { return d.measure(2).qNum; })*0.98;
		
		var xMin2 = xMin == xMax ? xMin*0.5 : xMin;
		var xMax2 = xMin == xMax ? xMax*1.5 : xMax;
		var yMin2 = yMin == yMax ? yMin*0.5 : yMin;
		var yMax2 = yMin == yMax ? yMax*1.5 : yMax;
	   
		 x.domain([xMin2, xMax2]).nice();
		 y.domain([yMin2, yMax2]).nice();    
	}else{
		
		var xMaxS, xMinS, yMaxS, yMinS;
		
		 xMax = d3.max(data, function(d) { return d.measure(1).qNum; })*1.02;
		xMaxS = d3.max(data, function(d) { return d.measure(3).qNum; })*1.02;
		
		xMin = d3.min(data, function(d) { return d.measure(1).qNum; })*0.98;
		xMinS = d3.min(data, function(d) { return d.measure(3).qNum; })*0.98;
		
		yMax = d3.max(data, function(d) { return d.measure(2).qNum; })*1.02;
		yMaxS = d3.max(data, function(d) { return d.measure(4).qNum; })*1.02;
		
		yMin = d3.min(data, function(d) { return d.measure(2).qNum; })*0.98;
		yMinS = d3.min(data, function(d) { return d.measure(4).qNum; })*0.98;
		
		xMax = (xMax > xMaxS) ? xMax : xMaxS;
		xMin = (xMin < xMinS) ? xMin : xMinS;
		
		yMax = (yMax > yMaxS)? yMax : yMaxS;
		yMin = (yMin < yMinS)? yMin : yMinS;
		
		var xMin2 = xMin == xMax ? xMin*0.5 : xMin;
		var xMax2 = xMin == xMax ? xMax*1.5 : xMax;
		var yMin2 = yMin == yMax ? yMin*0.5 : yMin;
		var yMax2 = yMin == yMax ? yMax*1.5 : yMax;
	   
		 x.domain([xMin2, xMax2]).nice();
		 y.domain([yMin2, yMax2]).nice();    
		
	}
	 
  var color = d3.scale.category20c();

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .tickSize(-height)
      .tickFormat(d3.format(".2s")); 

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")  
      .tickSize(-width)       
      .tickFormat(d3.format(".2s"));

  var zoomBeh = d3.behavior.zoom()
      .x(x)
      .y(y)
      .scaleExtent([0, 500])
      .on("zoom", zoom);
	/*
   var button = d3.select("#" + id) 
      .append("input")
      .attr("type", "button")
      .attr("name", "reset")
      .attr("value", "Reset Zoom")
      .attr("float", "left"); 
	*/
	
	var showHistoryButton = d3.select("#" + id) 
		.append("input")
		.attr("Id", "btnShowHistory")
		.attr("type", "button")
		.attr("name", "showHistory")
		.attr("value", "Show Prior Measure")
		.attr("float", "left"); 
		
	var resetButton = d3.select("#" + id) 
      .append("input")
	  .attr("Id", "btnReset")
      .attr("type", "button")
      .attr("name", "reset")
      .attr("value", "Reset")
      .attr("float", "left"); 
	
  var svg = d3.select("#" + id)    
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
     .attr("transform", "translate(" + margin.left + "," + margin.top + ")") 
     .call(zoomBeh);

  svg.append("rect")
    .attr("width", width)
    .attr("height", height);

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .append("text")
    .attr("class", "label")
    .attr("x", width * .5)
    .attr("y", margin.bottom - 10)
    .style("text-anchor", "end")
    .text(senseUtils.getMeasureLabel(1,layout));

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .append("text")
    .attr("class", "label")
    .attr("transform", "rotate(-90)")
	.attr("x", - (height * .4))
    .attr("y", -margin.left)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text(senseUtils.getMeasureLabel(2,layout));  
	
	data = data.sort(function(a,b){
			return (a[2].qNum - b[2].qNum);
		})
	
  var plot = svg.append("svg")
    .classed("objects", true)
      .attr("width", width)
      .attr("height", height);  

	var lineFunc = d3.svg.line()
	.x(function(d) { return  x(d.x); })
	.y(function(d) { return y(d.y); })
	.interpolate("linear");
	
	plotChartWith(data);	
	
	if (_this.options.showPriorMeaures){
		showHistory();
	}
    /*
     var legend = svg.selectAll(".legend")
        .data(color.domain())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });         

    legend.append("circle")
        .attr("r", 5)
        .attr("cx", width + 25)
        .attr("fill", color);

    legend.append("text")
        .attr("x", width + 32)
        .attr("dy", ".35em")
        .text(function(d) { return d; });
	*/
    //d3.select("input").on("click", change);
	
	d3.select("#btnShowHistory").on("click", function(){
		_this.options.showPriorMeaures = true;
		_this.paint($element, layout);
	});
	
	d3.select("#btnReset").on("click", function(){
		_this.options.showPriorMeaures = false;
		_this.paint($element, layout);
	});
	
	function plotChartWith(data){
			plot.selectAll(".dot")
			.data(data)
		  .enter().append("circle")
			.attr("class", "dot "+classDim)
			.attr("transform", transform)
			.attr("id", function(d) { return d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-"); })
			.attr("r", function(d) { return 5; })
			.style("fill", function(d, i) { 
					var percentile = ((i+1)/data.length);
					
					if (percentile < 0.75){
						return "#d9d9d9";
					}else if (percentile < 0.90){
						return "#969696";
					}else if (percentile >= 0.90){
						return "black";
					}
					return "";
			})
			.on("click", function(d) {d.dim(1).qSelect();})
			.on("mouseover", function(d){
			  d3.selectAll($("."+classDim+"#"+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-"))).classed("highlight",true);
				  d3.selectAll($("."+classDim+"[id!="+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-")+"]")).classed("dim",true);
			  })
			  .on("mouseout", function(d){
				  d3.selectAll($("."+classDim+"#"+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-"))).classed("highlight",false);
				  d3.selectAll($("."+classDim+"[id!="+d.dim(1).qText.replace(/[^A-Z0-9]+/ig, "-")+"]")).classed("dim",false);
			  })
				.append("title")
				.html(function(d) {return senseUtils.getDimLabel(1,layout) + ": " + d.dim(1).qText 
						+ "<br/>" + senseUtils.getMeasureLabel(1,layout) + ": " + d.measure(1).qText
						+ "<br/>" + senseUtils.getMeasureLabel(2,layout) + ": " + d.measure(2).qText
						+ "<br/>Total FI Gross: " + (d[1].qNum * d[2].qNum).toFixed(2)
						  });
		
			spotLightData = getSpotlightCoordinates(data);
			
			drawSpotlight(spotLightData);
			
			//-------------Draw Contour lines--------------------
			 var yArray, xArray, centerCord, indexer;
			
			$.each([.90, .75], function(i,v) {		
			 yArray = [], xArray = [];
			 centerCord = Math.round((xMax * v) * (yMax * v),0);
			 indexer = 1.02;
				
			 while (Math.ceil(yMax,0) > (centerCord/(xMax * indexer))){
				 xArray.push((xMax * indexer));
				 yArray.push(centerCord/(xMax * indexer));
				 indexer = (indexer - .01)
			 }
			 
			var reformContourData = [{ y : yArray, x : xArray}].map(function(d) {
				return d3.range(d.x.length).map(function(i) {
				  return {x: d.x[i], y: d.y[i]};
				});
			  });
			  
			 drawContourLines(reformContourData[0]);	
			});
		//-----------------------------------------------------
	}
	
	function drawContourLines(contourLineData){
		
		plot.append("path")
		  .attr("d", lineFunc(contourLineData))
		  .on("mouseover", function(d, i) {
			d3.select(this).style("stroke", "yellow");
		  })
		  .on("mouseout", function(d, i) {
			d3.select(this).style("stroke", "#d9d9d9");
		  })
		  .style("fill", "none")
		  .style("stroke", "#d9d9d9")
		  .style("stroke-width", 2);
	  
	}

	function getSpotlightCoordinates(data) {
		
		var MIN_SPOTLIGHT_DATACOUNT = 3;
		
		var orderedData = data.sort(function(a,b){
			return ((b[1].qNum * b[2].qNum) - (a[1].qNum * a[2].qNum));
		}); // sorting data table by total profit
		
		var tenPercentDataCount = orderedData.length * .1,
		spData;
		
		if (tenPercentDataCount < MIN_SPOTLIGHT_DATACOUNT){
			if (orderedData.length < MIN_SPOTLIGHT_DATACOUNT){
				spData = orderedData;
			}
			else{
				spData = orderedData.slice(0,MIN_SPOTLIGHT_DATACOUNT);
			}
		}
		else{
			spData = orderedData.slice(0,tenPercentDataCount-1);
		}
		
		var dataLength = spData.length;
		
		var meanX = spData.reduce(function(a,b){ return ((b[1].qNum * b[2].qNum) + a); }, 0) / spData.reduce(function(a,b){ return (b[2].qNum + a); }, 0);
		
		var meanY = spData.reduce(function(a,b){ return (b[2].qNum + a); }, 0) / dataLength;
		
		var xAxisDataArray = spData.map(function(v){ return v[1].qNum; });
		
		var dX = standardDeviation(xAxisDataArray);
		
		var yAxisDataArray = spData.map(function(v){ return v[2].qNum; });
		
		var dY = standardDeviation(yAxisDataArray);
		
		return {
			x: Math.round(meanX),
			y: Math.round(meanY),
			dx: (dX), // One standard deviation on x axis
			dy: (dY)   // One standard deviation on y axis
		}
	}
	
	function drawSpotlight(spc){
		
		var lineData =[{ "x": x(spc.x - spc.dx),  "y": y(spc.y + spc.dy)}, { "x": x(spc.x + spc.dx),  "y": y(spc.y + spc.dy)},
					{ "x": x(spc.x + spc.dx),  "y": y(spc.y + spc.dy)}, { "x": x(spc.x + spc.dx),  "y": y(spc.y - spc.dy)},
					{ "x": x(spc.x + spc.dx),  "y": y(spc.y - spc.dy)}, { "x": x(spc.x - spc.dx),  "y": y(spc.y - spc.dy)},
					{ "x": x(spc.x - spc.dx),  "y": y(spc.y - spc.dy)}, { "x": x(spc.x - spc.dx),  "y": y(spc.y + spc.dy)}
					];
	// console.log(lineData);
		 //This is the accessor function we talked about above
		var lineFunction = d3.svg.line()
								  .x(function(d) { return d.x; })
								  .y(function(d) { return d.y; })
								 .interpolate("linear");

		var lineGraph = plot.append("path")
								.attr("d", lineFunction(lineData))
								.attr("stroke", "black")
								.attr("stroke-width", 2)
								.attr("fill", "none");
		
	}
	
    function change() {
      
      var xMax3 = d3.max(data, function(d) { return d.measure(1).qNum; })*1.02;
      var xMin3 = d3.min(data, function(d) { return d.measure(1).qNum; })*0.98;
      var yMax3 = d3.max(data, function(d) { return d.measure(2).qNum; })*1.02;
      var yMin3 = d3.min(data, function(d) { return d.measure(2).qNum; })*0.98;

      
      zoomBeh
        .x(x.domain([xMin3, xMax3]).nice())
        .y(y.domain([yMin3, yMax3]).nice());

      var svg = d3.select("#" + id).transition();

      svg.select(".x.axis").duration(750).call(xAxis).select(".label").text(senseUtils.getMeasureLabel(1,layout));
      svg.select(".y.axis").duration(750).call(yAxis).select(".label").text(senseUtils.getMeasureLabel(2,layout));

      plot.selectAll(".dot").transition().duration(1000).attr("transform", transform);
	  
	  resetSpotlightArea();
    }
	
	function resetSpotlightArea(){
		d3.selectAll("path").remove();
		drawSpotlight(spotLightData);
	}

    function zoom() {
      svg.select(".x.axis").call(xAxis);
      svg.select(".y.axis").call(yAxis);

      svg.selectAll(".dot")
          .attr("transform", transform);
		
	  resetSpotlightArea();
    }

    function transform(d) {    
      
      return "translate(" + x(d.measure(1).qNum) + "," + y(d.measure(2).qNum) + ")";
    }
	
	function standardDeviation(values){
	  var avg = average(values);
	  
	  var squareDiffs = values.map(function(value){
		var diff = value - avg;
		var sqrDiff = diff * diff;
		return sqrDiff;
	  });
	  
	  var avgSquareDiff = average1(squareDiffs);

	  var stdDev = Math.sqrt(avgSquareDiff);
	  return stdDev;
	}

	function average(data){
	  var sum = data.reduce(function(sum, value){
		return sum + value;
	  }, 0);

	  var avg = sum / data.length;
	  return avg;
	}
	
	function average1(data){
	  var sum = data.reduce(function(sum, value){
		return sum + value;
	  }, 0);

	  var avg = sum / (data.length-1);
	  return avg;
	}
	
	function showHistory(){
	
	var histData = data.sort(function(a,b){
			return (a[4].qNum - b[4].qNum);
		})
	
	 plot.selectAll(".call")
	.data(histData)
	.enter().append("circle")
	.attr("class", "call "+classDim)
	.attr("transform", transformH)
	.attr("id", function(d) { return d.measure(3).qText.replace(/[^A-Z0-9]+/ig, "-"); })
	.attr("r", function(d) { return  5; })
	.style("fill", function(d, i) { 
					var percentile = ((i+1)/histData.length);
					
					if (percentile < 0.75){
						return "#c6dbef";
					}else if (percentile < 0.90){
						return "#6baed6";
					}else if (percentile >= 0.90){
						return "#3182bd";
					}
					return "";
			})
	.on("click", function(d) {d.dim(1).qSelect();})
	.append("title")
				.html(function(d) {return senseUtils.getDimLabel(1,layout) + ": " + d.dim(1).qText 
						+ "<br/>" + senseUtils.getMeasureLabel(3,layout) + ": " + d.measure(3).qText
						+ "<br/>" + senseUtils.getMeasureLabel(4,layout) + ": " + d.measure(4).qText
						+ "<br/>Total Prior FI Gross: " + (d[3].qNum * d[4].qNum).toFixed(2)
						  })
	.each(function(d,i){
		
		plot.append("svg:defs").append("svg:marker")
		.attr("id", "triangle" + i)
		.attr("refX", 6)
		.attr("refY", 6)
		.attr("markerWidth", 30)
		.attr("markerHeight", 30)
		.attr("orient", "auto")
		.append("path")
		.attr("d", "M 0 0 12 6 0 12 3 6")
		.style("fill", "black");
		
		plot.append("line")
			.attr("x1",x(d.measure(3).qNum))
			.attr("y1",y(d.measure(4).qNum))
			.attr("x2",x(d.measure(1).qNum * (0.97)))
			.attr("y2",y(d.measure(2).qNum * (0.97)))
			.attr("stroke-width",1)
			.attr("stroke","black")
			.attr("marker-end", "url(#triangle" + i + ")");
		
	});
		
	}
	
	function transformH(d) { 
      return "translate(" + x(d.measure(3).qNum) + "," + y(d.measure(4).qNum) + ")";
	}
}

